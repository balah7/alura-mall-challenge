package io.github.balah7.shopping.models;

import java.math.BigDecimal;
import java.util.Locale;

public class Compra implements Comparable<Compra> {
    public String titulo;

    public int unidades;
    public BigDecimal valor;

    public Compra(String titulo, int unidades, BigDecimal valor) {
        this.titulo = titulo;
        this.valor = valor;
        if (unidades <= 0) {
            this.unidades = 1;
        } else {
            this.unidades = unidades;
        }
    }

    public BigDecimal valorTotal() {
        return valor.multiply(BigDecimal.valueOf(unidades));
    }

    @Override
    public String toString() {
        String unidade = (unidades == 1) ? "Unidade" : "Unidades";
        return String.format("%s (%d %s) - $%s", titulo.toUpperCase(Locale.ROOT), unidades, unidade, valorTotal());
    }

    @Override
    public int compareTo(Compra o) {
        return o.valorTotal().compareTo(this.valorTotal());
    }
}