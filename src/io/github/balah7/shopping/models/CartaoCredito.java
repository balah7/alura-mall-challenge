package io.github.balah7.shopping.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CartaoCredito {
    private final BigDecimal limite;
    private BigDecimal saldo;
    private final List<Compra> historico = new ArrayList<>();

    public CartaoCredito(BigDecimal limite) {
        if (limite.compareTo(BigDecimal.ZERO) <= 0) {
            this.limite = BigDecimal.ONE;
            this.saldo = BigDecimal.ONE;
        } else {
            this.limite = limite;
            this.saldo = limite;
        }
    }

    public boolean efetuarCompra(Compra compra) {
        if (compra.valorTotal().compareTo(this.limite) > 0 || compra.valorTotal().compareTo(this.saldo) > 0) {
            System.out.println("Saldo insuficiente!");

            return false;
        } else {
            this.saldo = this.saldo.subtract(compra.valorTotal());
            System.out.println("Compra realizada!");

            historico.add(compra);

            return true;
        }
    }

    public BigDecimal getLimite() {
        return limite;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public List<Compra> getHistorico() {
        this.historico.sort(null);
        return historico;
    }
}