package io.github.balah7.shopping.main;

import io.github.balah7.shopping.models.CartaoCredito;
import io.github.balah7.shopping.models.Compra;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        CartaoCredito cartao;

        try {
            System.out.println("Digite o limite do cartão: ");
            cartao = new CartaoCredito(new BigDecimal(scan.nextLine()));
        } catch (NumberFormatException e) {
            System.out.println("Erro: Valor do cartão inválido. Certifique-se de digitar um número válido.");
            return;
        }

        byte ativo = 1;
        while (ativo == 1) {
            try {
                System.out.println("Digite a descrição do produto: ");
                String descricao = scan.nextLine().trim();

                System.out.println("Digite a quantidade de unidades do produto: ");
                String inputUnidades = scan.nextLine();
                int unidades = 1;
                if (!inputUnidades.isEmpty() || !inputUnidades.isBlank()) {
                    unidades = Integer.parseInt(inputUnidades);
                }

                System.out.println("Digite o valor da unidade: ");
                BigDecimal valor = new BigDecimal(scan.nextLine());
                if (valor.compareTo(BigDecimal.ZERO) <= 0) {
                    valor = BigDecimal.ONE;
                }

                Compra compra = new Compra(descricao, unidades, valor);

                if (cartao.efetuarCompra(compra)) {
                    try {
                        System.out.println("Digite 1 para continuar ou qualquer número para parar: ");
                        ativo = scan.nextByte();
                        scan.nextLine();
                    } catch (InputMismatchException e) {
                        System.out.println("Erro: Entrada inválida. Por favor, forneça um valor correto para continuar.");
                        return;
                    }

                    if (ativo != 1) {
                        encerrarPrograma(cartao);
                    }
                } else {
                    encerrarPrograma(cartao);
                    ativo = 0;
                }
            } catch (NumberFormatException e) {
                System.out.println("Erro: Entrada Inválida. Certifique-se de digitar um número válido.");
                return;
            }
        }
    }


    public static void encerrarPrograma(CartaoCredito cartao) {
        if (!cartao.getHistorico().isEmpty()) {
            System.out.println("******************************");
            System.out.println("COMPRAS REALIZADAS:");

            for (Compra i : cartao.getHistorico()) {
                System.out.println(i);
            }

            System.out.println("Saldo restante: " + cartao.getSaldo().toString() + "$ (Total: " + cartao.getLimite() + "$)");
            System.out.println("******************************");
        } else {
            System.out.println("Nenhuma compra realizada!");
        }
    }
}