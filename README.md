# Alura Mall Chanlenge

This is a simple shopping cart application developed in Java. It allows users to make purchases using a credit card with
a specified limit. The application prompts the user to enter the credit card limit and then allows them to make
purchases by providing the product description, quantity, and unit price.

Challenge taken from Alura's Java training

## Usage

1. Clone the repository to your local machine.
2. Compile the Java files using a Java compiler.
3. Run the `Main` class to start the application.
4. Follow the prompts to enter the credit card limit and make purchases.

## Features

- Allows users to set a credit card limit.
- Handles input validation for credit card limit, product description, quantity, and unit price.
- Displays the list of purchases made along with the remaining balance after each purchase.

## Dependencies

- Java Development Kit (JDK)
- Java Compiler

## Contributors

- [Balah7](https://gitlab.com/balah7/)